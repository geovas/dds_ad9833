// ----------------------------------------------------------------
//                    ����������� ���������
// ----------------------------------------------------------------

#include <stm32f37x.h>   // �������� ���������� STM32F373RC
#include <string.h>		   // ������ �� ��������
#include <stdbool.h>     // ������ � ����������� ���� boolean
#include <stdio.h>       // ������� printf/scanf � �.�.

#include "gpio.h"        // ��� ���������� ��� ������ � GPIO
#include "timer.h"       // ��� ���������� ��� ������ � ���������
#include "display.h"     // ��� ���������� ��� ������ � LCD
#include "dds.h"				 // ��� ���������� ��� ������ � DDS-����������� �� SPI

// ----------------------------------------------------------------
//               define-���������� � ���������
// ----------------------------------------------------------------



// ----------------------------------------------------------------
//                        ����������
// ----------------------------------------------------------------

											

// ----------------------------------------------------------------
//                  �������� �������� (main)
// ----------------------------------------------------------------

int main(void)
{
	volatile bool      			new_sign = true;					// ����� �� �������� ������ �� DDS-���������?
	volatile unsigned short dds_type = DDS_TYPE_SIN;	// ������� ����� ������� DDS-����������
	volatile long			 			dds_frq  = 100;						// ������� ������� ������� DDS-����������
	volatile long						dds_pow  = 1;							// ������� ��������� ������� DDS-����������

	char buf[100];																		// ����� ��� sprintf
	char ch;																					// �������� ���������� (��� ���, ��� � ��)

	// --------------------------------------------------------------
	
	timer_init();	// ������������� �������� (��������� ��������)
	key_init();		// ������������� ����������
	led_init();		// ������������� �����������
	lcd_init();		// ������������� ���
	dds_init();		// ������������� DDS-����������

	// --------------------------------------------------------------
	
	lcd_write_string(0,0,"DDS-��������� v1.0",0x00);
	dds_write(dds_frq*dds_pow, dds_type);

	for(;;)
	{
		// ��������� ������ ���������� �� ��� �������
		if (key_read(KEY_F1))
		{
			dds_type = DDS_TYPE_SIN;
			new_sign = true;
			while (key_read(KEY_F1)) __ASM("NOP");
		}
		if (key_read(KEY_F2))
		{
			dds_type = DDS_TYPE_TRI;
			new_sign = true;
			while (key_read(KEY_F2)) __ASM("NOP");
		}
		if (key_read(KEY_F3))
		{
			dds_type = DDS_TYPE_SQR;
			new_sign = true;
			while (key_read(KEY_F3)) __ASM("NOP");
		}		
		
		// ��������� ������ ���������� �� �������
		if (key_read(KEY_RIGHT))
		{
			switch (dds_pow)
			{
				case 1:			 	dds_pow = 1000;			break;
				case 1000:	 	dds_pow = 1000000;	break;
				case 1000000: dds_pow = 1;				break;
			}
			new_sign = true;
			while (key_read(KEY_RIGHT)) __ASM("NOP");
		}
		if (key_read(KEY_LEFT))
		{
			switch (dds_pow)
			{
				case 1:			 	dds_pow = 1000000;	break;
				case 1000:		dds_pow = 1;				break;
				case 1000000: dds_pow = 1000;			break;
			}
			new_sign = true;
			while (key_read(KEY_LEFT)) __ASM("NOP");
		}
		if (key_read(KEY_UP))
		{
			if 			(dds_frq < 10)  dds_frq += 1;
			else if (dds_frq < 100) dds_frq += 5;
			else										dds_frq += 50;
			if (dds_frq > 950)  		dds_frq = 950;
			
			new_sign = true;
			while (key_read(KEY_UP)) __ASM("NOP");			
		}
		if (key_read(KEY_DOWN))
		{
			if 			(dds_frq <= 10) 	dds_frq -= 1;
			else if (dds_frq <= 100)	dds_frq -= 5;
			else											dds_frq -= 50;
			if (dds_frq < 0) 	 				dds_frq = 0;
			
			new_sign = true;
			while (key_read(KEY_DOWN)) __ASM("NOP");			
		}
		
		
		// �������� ������ �� ���������
		if (new_sign)
		{
			new_sign = false;
			dds_write(dds_frq*dds_pow, dds_type);
			
			// ������� �������
			switch(dds_pow)
			{
				case 1:				ch = ' '; break;
				case 1000:		ch = '�'; break;
				case 1000000: ch = '�'; break;				
			}
			
			sprintf(buf, "�������: %03d %c��", dds_frq, ch);
			lcd_write_string(3,1,buf, 0x00);
			
			// ��� �������
			switch (dds_type)
			{
				case DDS_TYPE_SIN: lcd_write_string(5,1, "��� �������: SIN", 0x00); break;
				case DDS_TYPE_TRI: lcd_write_string(5,1, "��� �������: TRI", 0x00); break;
				case DDS_TYPE_SQR: lcd_write_string(5,1, "��� �������: SQR", 0x00); break;
			}
			
		}
	}
}

// ----------------------------------------------------------------
// THE END
// ----------------------------------------------------------------
