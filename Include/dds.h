#ifndef __DDS_H
#define __DDS_H

// ----------------------------------------------------------------
//    ����������� ��������� (���� ��� ��� �� ������� � main-�)
// ----------------------------------------------------------------

#include "timer.h"      // ��� ���������� ��� ������ � ���������
#include <string.h>			// ������ �� ��������
#include <stdio.h>      // ������� printf/scanf � �.�.
#include <math.h>				// �������������� ���������� (������� pow() )


// ----------------------------------------------------------------
//                   define  - ����������
// ----------------------------------------------------------------

// ��������� ����� GPIO
#define DDS_RCC					RCC_AHBPeriph_GPIOC

#define DDS_FSNC_PIN 		GPIO_Pin_0
#define DDS_FSNC_PORT 	GPIOC

#define DDS_SCLK_PIN 		GPIO_Pin_1
#define DDS_SCLK_PORT 	GPIOC

#define DDS_MOSI_PIN 		GPIO_Pin_2
#define DDS_MOSI_PORT 	GPIOC


// ----------------------------------------------------------------

// ��������� ����������
#define DDS_TYPE_SIN 		0x2000 // ��� ��������� ������� - �����
#define DDS_TYPE_SQR 		0x2028 // ��� ��������� ������� - �������������
#define DDS_TYPE_TRI 		0x2002 // ��� ��������� ������� - ����������� 


// ----------------------------------------------------------------
//                         �������
// ----------------------------------------------------------------

void dds_command(unsigned short comm) // �������� ������� �� ��������� �� SPI
{
	unsigned char i;
	
	// ������ ����� (�����)
	GPIO_ResetBits(DDS_FSNC_PORT, DDS_FSNC_PIN);
	GPIO_SetBits(  DDS_SCLK_PORT, DDS_SCLK_PIN);
	delay_ms(2);

	// �������� ������
	for (i=0; i<16; i++)
	{
		if (comm & 0x8000) GPIO_SetBits(  DDS_MOSI_PORT, DDS_MOSI_PIN);
		else							 GPIO_ResetBits(DDS_MOSI_PORT, DDS_MOSI_PIN);
		
		GPIO_ResetBits(DDS_SCLK_PORT, DDS_SCLK_PIN);
		delay_ms(2);
		
		GPIO_SetBits(  DDS_SCLK_PORT, DDS_SCLK_PIN);
		comm = comm << 1;
	}
	
	// ����� ����� (����)
	GPIO_SetBits(  DDS_FSNC_PORT, DDS_FSNC_PIN);
	GPIO_SetBits(  DDS_SCLK_PORT, DDS_SCLK_PIN);
	delay_ms(2);
	
	return;
}


void dds_init(void) // ������������� DDS-����������
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(DDS_RCC, ENABLE);
	
	// FSNC
	GPIO_InitStructure.GPIO_Pin = DDS_FSNC_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(DDS_FSNC_PORT, &GPIO_InitStructure);
	
	// SCLK
	GPIO_InitStructure.GPIO_Pin = DDS_SCLK_PIN;
	GPIO_Init(DDS_SCLK_PORT, &GPIO_InitStructure);
	
	// MOSI
	GPIO_InitStructure.GPIO_Pin = DDS_MOSI_PIN;
	GPIO_Init(DDS_MOSI_PORT, &GPIO_InitStructure);
	
	// ����� ���������� ???
	delay_ms(500);
	dds_command(1 << 8);
	delay_ms(500);
	return;
}


void dds_write(long frq, unsigned short type) // �������� ������ �� DDS-���������
{
	long FreqWord = (frq * pow(2, 28)) / 25e6f;
	int MSB, LSB;
	

  MSB = (unsigned short)((FreqWord & 0xFFFC000) >> 14) | 0x4000;
  LSB = (unsigned short)( FreqWord & 0x3FFF)					 | 0x4000;
 
  dds_command(0x2100);   
  dds_command(LSB);    // ������ LSB �� �������
  dds_command(MSB);    // ������ MSB �� �������
  dds_command(0xC000); // ������ ����
  dds_command(type);   // ��������� ���� ������� - SIN, SQR ��� TRI
	
	return;
}


// ----------------------------------------------------------------
#endif
