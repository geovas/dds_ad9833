#ifndef __TIMER_H
#define __TIMER_H

// ----------------------------------------------------------------
//                   define  - ����������
// ----------------------------------------------------------------

#define TIM_US_RCC	RCC_APB1Periph_TIM3 		// ������� ������������ ������� (���)
#define TIM_US_RCF  RCC_APB1PeriphClockCmd	// ������� � ������� ������� ���. ������������ TIM_US
#define TIM_US			TIM3										// ������ ������������, ��� delay_5us

// ----------------------------------------------------------------
//                      ����������
// ----------------------------------------------------------------

volatile unsigned int time_ms = 0; 			// ������� ����� � ������������

// ----------------------------------------------------------------
//             ������� � ���������� ����������
// ----------------------------------------------------------------

void delay_ms(unsigned int t) // �������� t-����������
{
	volatile unsigned int now = time_ms;
	while (time_ms <= (now+t)) __ASM("NOP");
	return;
}

void delay_5us(void) // �������� ������� 5 ��� (��� f = 72���)
{
	TIM_US->CNT	= 0x00;							// �������� ������
	TIM_US->CR1	 |= TIM_CR1_CEN;		// ��������� ������
	while ((TIM_US->CNT) < (72*5));	// ���� 72*5 ������ ���������� (��� 72��� - ��� 5���)
}

void timer_init(void) // ������������� ���������� �����
{
	// ��������� ������� �������������, ��� ����������������� ���������� "time_ms" � ������� "delay_ms()"
	SysTick->LOAD = SystemCoreClock/(1000)-1;
	SysTick->VAL  = SystemCoreClock/(1000)-1;
	SysTick->CTRL = SysTick_CTRL_CLKSOURCE_Msk | SysTick_CTRL_TICKINT_Msk | SysTick_CTRL_ENABLE_Msk;
	
	// ��������� ������� �������������, ��� ������� "delay_5us()"
	TIM_US_RCF(TIM_US_RCC, ENABLE);	// �������� ������������
	TIM_US->CR1	 |= TIM_CR1_CEN;		// ��������� ������
	
	return;
}

void SysTick_Handler(void) // ���������� ���������� �� ���������� �������
{
	time_ms++; // ������ 1�� - �������������� �������
	return;
}


// ----------------------------------------------------------------
#endif
