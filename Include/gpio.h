#ifndef __GPIO_H
#define __GPIO_H

// ----------------------------------------------------------------
//    ����������� ��������� (���� ��� ��� �� ������� � main-�)
// ----------------------------------------------------------------

#include <stdbool.h>    // ������ � ����������� ���� boolean
#include "timer.h"

// ----------------------------------------------------------------
//                      define-����������
// ----------------------------------------------------------------

// ����������
#define KEY_UP      0
#define KEY_DOWN    1
#define KEY_LEFT    2
#define KEY_RIGHT   3
#define KEY_ENTER   4
#define KEY_ESC     5
#define KEY_F1      6
#define KEY_F2      7
#define KEY_F3      8


#define KEY_RCC			RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOD | RCC_AHBPeriph_GPIOF

#define KEY_1_PIN		GPIO_Pin_7
#define KEY_1_PORT	GPIOF

#define KEY_2_PIN		GPIO_Pin_15
#define KEY_2_PORT	GPIOA

#define KEY_3_PIN		GPIO_Pin_10
#define KEY_3_PORT	GPIOC

#define KEY_A_PIN		GPIO_Pin_11
#define KEY_A_PORT	GPIOC

#define KEY_B_PIN		GPIO_Pin_12
#define KEY_B_PORT	GPIOC

#define KEY_C_PIN		GPIO_Pin_2
#define KEY_C_PORT	GPIOD


// ----------------------------------------------------------------

// ����������
#define LED_1       0x01
#define LED_2       0x02

#define LED_RCC     RCC_AHBPeriph_GPIOC

#define LED_1_PIN   GPIO_Pin_3
#define LED_1_PORT  GPIOC

#define LED_2_PIN   GPIO_Pin_3
#define LED_2_PORT  GPIOC

// ----------------------------------------------------------------

//��������� ����-��������
#define KEY_COUNT		25
#define KEY_THRESH	20

// ----------------------------------------------------------------
//                           �������
// ----------------------------------------------------------------

void key_init(void) // ������������� ����������
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(KEY_RCC, ENABLE);
	
	// ����� ���������� 1 - OUT
	GPIO_InitStructure.GPIO_Pin = KEY_1_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(KEY_1_PORT, &GPIO_InitStructure);
	
	// ����� ���������� 2 - OUT
	GPIO_InitStructure.GPIO_Pin = KEY_2_PIN;
	GPIO_Init(KEY_2_PORT, &GPIO_InitStructure);
	
	// ����� ���������� 3 - OUT
	GPIO_InitStructure.GPIO_Pin = KEY_3_PIN;
	GPIO_Init(KEY_3_PORT, &GPIO_InitStructure);
	
	// ����� ���������� A - IN
	GPIO_InitStructure.GPIO_Pin = KEY_A_PIN;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_IN;
	GPIO_Init(KEY_A_PORT, &GPIO_InitStructure);
	
	// ����� ���������� B - IN
	GPIO_InitStructure.GPIO_Pin = KEY_B_PIN;
	GPIO_Init(KEY_B_PORT, &GPIO_InitStructure);
	
	// ����� ���������� C - IN
	GPIO_InitStructure.GPIO_Pin = KEY_C_PIN;
	GPIO_Init(KEY_C_PORT, &GPIO_InitStructure);
	
	return;
}

bool key_read(unsigned int key) // ������ ������ � ����������
{
	volatile unsigned int i, arr[9] = {0};
	
	for (i=0; i<KEY_COUNT; i++)
	{
		GPIO_SetBits(  KEY_1_PORT, KEY_1_PIN);
		GPIO_ResetBits(KEY_2_PORT, KEY_2_PIN);
		GPIO_ResetBits(KEY_3_PORT, KEY_3_PIN);
		if (GPIO_ReadInputDataBit(KEY_A_PORT, KEY_A_PIN)) arr[KEY_ENTER]++;
		if (GPIO_ReadInputDataBit(KEY_B_PORT, KEY_B_PIN)) arr[KEY_DOWN]++;
		if (GPIO_ReadInputDataBit(KEY_C_PORT, KEY_C_PIN)) arr[KEY_RIGHT]++;
		
		GPIO_ResetBits(KEY_1_PORT, KEY_1_PIN);
		GPIO_SetBits(  KEY_2_PORT, KEY_2_PIN);
		if (GPIO_ReadInputDataBit(KEY_A_PORT, KEY_A_PIN)) arr[KEY_ESC]++;
		if (GPIO_ReadInputDataBit(KEY_B_PORT, KEY_B_PIN)) arr[KEY_LEFT]++;
		if (GPIO_ReadInputDataBit(KEY_C_PORT, KEY_C_PIN)) arr[KEY_UP]++;
		
		GPIO_ResetBits(KEY_2_PORT, KEY_2_PIN);
		GPIO_SetBits(  KEY_3_PORT, KEY_3_PIN);
		if (GPIO_ReadInputDataBit(KEY_A_PORT, KEY_A_PIN)) arr[KEY_F1]++;
		if (GPIO_ReadInputDataBit(KEY_B_PORT, KEY_B_PIN)) arr[KEY_F2]++;
		if (GPIO_ReadInputDataBit(KEY_C_PORT, KEY_C_PIN)) arr[KEY_F3]++;
	}
	
	if (arr[key] >= KEY_THRESH) return true; // ������ ������������� ������
	return false;
}

void key_wait(unsigned int key) // ����, ����� ������ ����� ������ � ����� ��������
{
	while (!key_read(key)) __ASM("NOP");
	delay_ms(5);
	while ( key_read(key)) __ASM("NOP");
}

void led_init(void) // ������������� �����������
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(LED_RCC, ENABLE);
	
	// ������ ���������
	GPIO_InitStructure.GPIO_Pin = LED_1_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	
	GPIO_Init(LED_1_PORT, &GPIO_InitStructure);
	
	// ������ ���������
	GPIO_InitStructure.GPIO_Pin = LED_2_PIN;
	GPIO_Init(LED_2_PORT, &GPIO_InitStructure);
	return;
}

void led_write(unsigned int led, bool state) // ���/���� ����������
{
	switch (led)
	{
		case LED_1:
			if (state) GPIO_SetBits(  LED_1_PORT, LED_1_PIN);
			else       GPIO_ResetBits(LED_1_PORT, LED_1_PIN);
			break;
		case LED_2:
			if (state) GPIO_SetBits(  LED_2_PORT, LED_2_PIN);
			else       GPIO_ResetBits(LED_2_PORT, LED_2_PIN);
			break;
	}
	return;
}

// ----------------------------------------------------------------
#endif
