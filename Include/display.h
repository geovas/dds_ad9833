#ifndef __DISPLAY_H
#define __DISPLAY_H

// ----------------------------------------------------------------
//    ����������� ��������� (���� ��� ��� �� ������� � main-�)
// ----------------------------------------------------------------

#include <string.h>			// ������ �� ��������
#include <stdio.h>      // ������� printf/scanf � �.�.

// ----------------------------------------------------------------
//                        define  - ����������
// ----------------------------------------------------------------

// ������������ ����� �����/������:
#define LCD_RCC			RCC_AHBPeriph_GPIOA | RCC_AHBPeriph_GPIOB | RCC_AHBPeriph_GPIOC | RCC_AHBPeriph_GPIOE

// -----

#define LCD_D0_PIN	GPIO_Pin_0
#define LCD_D1_PIN	GPIO_Pin_1
#define LCD_D2_PIN	GPIO_Pin_2
#define LCD_D3_PIN	GPIO_Pin_3
#define LCD_D4_PIN	GPIO_Pin_4
#define LCD_D5_PIN	GPIO_Pin_5
#define LCD_D6_PIN	GPIO_Pin_6
#define LCD_D7_PIN	GPIO_Pin_7
#define LCD_D_PORT	GPIOB

// -----

#define LCD_A0_PIN	GPIO_Pin_9
#define LCD_A0_PORT	GPIOC

#define LCD_RW_PIN	GPIO_Pin_8
#define LCD_RW_PORT	GPIOC

#define LCD_RS_PIN	GPIO_Pin_7
#define LCD_RS_PORT	GPIOC

#define LCD_E1_PIN	GPIO_Pin_9
#define LCD_E1_PORT	GPIOE

#define LCD_E2_PIN	GPIO_Pin_8
#define LCD_E2_PORT	GPIOE

#define LCD_E_PIN		GPIO_Pin_8
#define LCD_E_PORT	GPIOA


// ----------------------------------------------------------------
//                  ���������� � ���������
// ----------------------------------------------------------------

extern const char Font_6x8_Data[]; 			 // ����� ������ ��� �������


// ----------------------------------------------------------------
//                     ������� (����������)
// ----------------------------------------------------------------

void lcd_write_bus(unsigned char b) // ������ ����� �� ���� D0..D7
{
	GPIO_SetBits(  LCD_D_PORT,  b);
	GPIO_ResetBits(LCD_D_PORT, ~b);
	return;
}

void lcd_write_dat(unsigned char dat) // ������ ������ �� ���� + �����
{
	GPIO_SetBits(  LCD_A0_PORT, LCD_A0_PIN);
	GPIO_ResetBits(LCD_RW_PORT, LCD_RW_PIN);
	GPIO_ResetBits(LCD_E_PORT,  LCD_E_PIN);
	delay_5us();
	
	lcd_write_bus(dat);
	delay_5us();
	GPIO_SetBits(  LCD_E_PORT,  LCD_E_PIN);
	delay_5us();
	GPIO_ResetBits(LCD_E_PORT, LCD_E_PIN);

	delay_5us();
	delay_5us();
	return;
}

void lcd_write_cmd(unsigned char dat) // ������ ������� �� ���� + �����
{
	GPIO_ResetBits(LCD_A0_PORT, LCD_A0_PIN);
	GPIO_ResetBits(LCD_RW_PORT, LCD_RW_PIN);
	GPIO_ResetBits(LCD_E_PORT,  LCD_E_PIN);
	delay_5us();
	
	lcd_write_bus(dat);
	delay_5us();
	GPIO_SetBits(  LCD_E_PORT,  LCD_E_PIN);
	delay_5us();
	GPIO_ResetBits(LCD_E_PORT, LCD_E_PIN);

	delay_5us();
	delay_5us();
	return;
}

void lcd_move_pos(unsigned char page, unsigned char addr) // ����������� �� ������ ������� ������
{
	lcd_write_cmd(0xb8 + page); // ��������� "��������"
	lcd_write_cmd(0x40 + addr); // ��������� "������"
	return;
}


// ----------------------------------------------------------------
//                     ������� (�������)
// ----------------------------------------------------------------

void lcd_clear(void) // ������� ������
{
	unsigned char i, j;
	
	GPIO_SetBits(  LCD_E1_PORT,  LCD_E1_PIN);
	GPIO_ResetBits(LCD_E2_PORT,  LCD_E2_PIN);
	for (i=0; i<8; i++)
	{
		lcd_move_pos(i,0);
		for (j=0; j<64; j++) lcd_write_dat(0x00);
	}
	
	GPIO_ResetBits(LCD_E1_PORT,  LCD_E1_PIN);
	GPIO_SetBits(  LCD_E2_PORT,  LCD_E2_PIN);
	for (i=0; i<8; i++)
	{
		lcd_move_pos(i,0);
		for (j=0; j<64; j++) lcd_write_dat(0x00);
	}
	return;
}

void lcd_init(void) // ������������� �������
{
	GPIO_InitTypeDef GPIO_InitStructure;
	RCC_AHBPeriphClockCmd(LCD_RCC, ENABLE);
	
	// ���� ������ D0..D7
	GPIO_InitStructure.GPIO_Pin  = LCD_D0_PIN | LCD_D1_PIN | LCD_D2_PIN | LCD_D3_PIN;
	GPIO_InitStructure.GPIO_Pin |= LCD_D4_PIN | LCD_D5_PIN | LCD_D6_PIN | LCD_D7_PIN;
	GPIO_InitStructure.GPIO_Speed = GPIO_Speed_50MHz;
	GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
	GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_Init(LCD_D_PORT, &GPIO_InitStructure);
	lcd_write_bus(0x00);
	
	// ����� A0 (�������/������)
	GPIO_InitStructure.GPIO_Pin = LCD_A0_PIN;
	GPIO_Init(LCD_A0_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(LCD_A0_PORT, LCD_A0_PIN);
	
	// ����� RW (������/������)
	GPIO_InitStructure.GPIO_Pin = LCD_RW_PIN;
	GPIO_Init(LCD_RW_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(LCD_RW_PORT, LCD_RW_PIN);
	
	// ����� RS (����� - ��������� ���������)
	GPIO_InitStructure.GPIO_Pin = LCD_RS_PIN;
	GPIO_Init(LCD_RS_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(LCD_RS_PORT, LCD_RS_PIN);
	
	// ����� E1 (����� ������� "���������")
	GPIO_InitStructure.GPIO_Pin = LCD_E1_PIN;
	GPIO_Init(LCD_E1_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(LCD_E1_PORT, LCD_E1_PIN);
	
	// ����� E2 (����� ������� "���������")
	GPIO_InitStructure.GPIO_Pin = LCD_E2_PIN;
	GPIO_Init(LCD_E2_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(LCD_E2_PORT, LCD_E2_PIN);
	
	// ����� E (������������� ������)
	GPIO_InitStructure.GPIO_Pin = LCD_E_PIN;
	GPIO_Init(LCD_E_PORT, &GPIO_InitStructure);
	GPIO_ResetBits(LCD_E_PORT, LCD_E_PIN);
	
	
	// ������������� �������
	delay_ms(10);
	GPIO_SetBits(LCD_RS_PORT, LCD_RS_PIN);
	delay_ms(10);

	// ������� ������ � ���� ������������ �������
	lcd_clear();
	GPIO_SetBits(  LCD_E1_PORT, LCD_E1_PIN);
	GPIO_SetBits(  LCD_E2_PORT, LCD_E2_PIN);
	lcd_write_cmd(0x3f);
	GPIO_ResetBits(LCD_E1_PORT, LCD_E1_PIN);
	GPIO_ResetBits(LCD_E2_PORT, LCD_E2_PIN);
	
	return;
}

void lcd_write_char(unsigned int y, unsigned int x, unsigned char ch, unsigned int inv) // ������ ������� �� �������
{
	unsigned int i;
	
	// ��� �������: 9 - ��� ����� �������� �� ����� �������� ������ (�� �����������)
	//              7 - ��� ����������� ������ ������� (�� �����������)
	//              ����� �� ���� "���������", �� �������� ����� ����� �� 1 ����. ������
	//              ��� ���� ������ ����� �������� �� ����� �����
	
	if (x < 9) // ����� �������� ������
	{
		GPIO_SetBits(  LCD_E1_PORT,  LCD_E1_PIN);
		GPIO_ResetBits(LCD_E2_PORT,  LCD_E2_PIN);
		x = x * 7 + 1;
	}
	else			 // ������ �������� ������
	{
		GPIO_ResetBits(LCD_E1_PORT,  LCD_E1_PIN);
		GPIO_SetBits(  LCD_E2_PORT,  LCD_E2_PIN);
		x = (x - 9) * 7;
	}
	
	lcd_move_pos(y,x);
	for (i=0; i<6; i++) lcd_write_dat(Font_6x8_Data[6*ch + i]^inv);
	lcd_write_dat(0x00); // ������ ����� ���������
	
	return;
}

void lcd_write_string(unsigned char y, unsigned char x, char *str, unsigned char inv) // ������ ������ �� �������
{
	unsigned int i, len = strlen((char*)str);
	for (i=0; i<len; i++) lcd_write_char(y,x+i,str[i],inv);
	return;
}

// ----------------------------------------------------------------
#endif
